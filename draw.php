<?php
#1
function drawTriangle()
{
    $rows = 4;
    for ($i = 1; $i <= $rows; $i++) {
        for ($j = 1; $j <= $i; $j++) {
            echo "* ";
        }
        echo "\n";
    }
}

drawTriangle();
echo "\n";
#2
function drawSquareEmpty()
{
    $stars = 5;
    for ($i = 0; $i < $stars; $i++) {
        for ($j = 0; $j < $stars; $j++) {

            if ($i == 0 || $i == $stars - 1 || $j == 0 || $j == $stars - 1) {
                echo "* ";
            } else {
                echo "  ";
            }
        }
        echo "\n";
    }
}


drawSquareEmpty();
echo "\n";

// #3
function drawSquare()
{
    $n = 5;
    for ($i = 0; $i < $n; $i++) {
        for ($j = 0; $j < $n; $j++) {
            echo "* ";
        }
        echo "\n";
    }
}
drawSquare();
echo "\n";
#4
function invertedTriangle()
{
    $rows = 5;

    for ($i = $rows; $i >= 1; $i--) {
        // In ra khoảng trắng trước các dấu sao
        for ($j = 1; $j <= $rows - $i; $j++) {
            echo '  ';
        }
        // In ra các dấu sao trên mỗi hàng
        for ($j = 1; $j <= 2 * $i - 1; $j++) {
            echo '* ';
        }
        // In ra dòng mới để chuyển sang hàng tiếp theo
        echo "\n";
    }
}
invertedTriangle();
echo "\n";
#5
function emptyTriangle()
{
    $rows = 5;

    for ($i = 1; $i <= $rows; $i++) {
        for ($j = 1; $j <= $rows - $i; $j++) {
            echo '  ';
        }
        if ($i == 1 || $i == $rows) {
            for ($j = 1; $j <= 2 * $i - 1; $j++) {
                echo '* ';
            }
        } else {
            echo '* ';
            for ($j = 1; $j <= 2 * $i - 3; $j++) {
                echo '  ';
            }
            echo '* ';
        }
        echo "\n";
    }
}
emptyTriangle();
